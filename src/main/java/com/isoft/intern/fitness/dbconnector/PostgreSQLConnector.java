package com.isoft.intern.fitness.dbconnector;

import com.isoft.intern.fitness.utils.constants.DataBaseConstants;
import com.isoft.intern.fitness.utils.constants.ExceptionConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Nikolay Uzunov
 */
public class PostgreSQLConnector
{
    private static final Logger LOGGER = LogManager.getLogger(PostgreSQLConnector.class.getName());
    private static Connection CONNECTION;
    private PostgreSQLConnector()
    {

    }

    public static Connection getConnection()
    {
        initializeConnection();
        return CONNECTION;
    }

    public static void initializeConnection()
    {
        try
        {
            try
            {
                Class.forName(DataBaseConstants.DRIVER);
            }
            catch (ClassNotFoundException e)
            {
                LOGGER.error("PostgreSql driver is not found! ", e);
            }

            CONNECTION = DriverManager.getConnection(
                    DataBaseConstants.FQN + DataBaseConstants.NAME, DataBaseConstants.USERNAME,
                    DataBaseConstants.PASSWORD);

            if (CONNECTION != null)
            {
                LOGGER.debug("Connection is successful!");
            }
            else
            {
                LOGGER.debug("Connection is not successful!");
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_MESSAGE, e);
        }
    }
}
