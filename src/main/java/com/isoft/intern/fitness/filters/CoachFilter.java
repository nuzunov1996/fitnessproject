package com.isoft.intern.fitness.filters;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.intern.fitness.models.Coach;
import com.isoft.intern.fitness.utils.constants.ExceptionConstants;
import com.isoft.intern.fitness.utils.validator.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Nikolay Uzunov
 */
@WebFilter(filterName = "CoachFilter", urlPatterns = {"/coach"})
public class CoachFilter implements Filter
{
    private static Logger LOGGER = LogManager.getLogger(Coach.class.getName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpServletRequest req = (HttpServletRequest) request;
        ObjectMapper objectMapper = new ObjectMapper();
        switch (req.getMethod().toUpperCase())
        {
            case "PUT":
            {
                try
                {
                    Coach coach = objectMapper.readValue(req.getReader(), Coach.class);
                    if (coach == null)
                    {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        return;
                    }
                    //TODO: CHECK FOR EXISTING ID'S
                    if (coach.getId() == 0||!Validator.isValidPhoneNumber(coach.getPhoneNumber()) || coach.getPhoneNumber() == null || coach.getName() == null ||
                            coach.getLastName() == null || coach.getAddress() == null )
                    {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        return;
                    }
                    req.setAttribute("coach", coach);
                }
                catch (JsonMappingException e)
                {
                    LOGGER.error(ExceptionConstants.JSON_MAPPING_EXCEPTION_MESSAGE , e);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            }break;
            case "DELETE":
            {
                try
                {
                    Coach coach = objectMapper.readValue(req.getReader(), Coach.class);
                    if (coach == null)
                    {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        return;
                    }
                    //TODO: CHECK FOR EXISTING ID
                    if (coach.getId() == 0 )
                    {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        return;
                    }
                    req.setAttribute("coach", coach);
                }
                catch (JsonMappingException e)
                {
                    LOGGER.error(ExceptionConstants.JSON_MAPPING_EXCEPTION_MESSAGE , e);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            }break;
            case "POST":
            {
                try
                {
                    Coach coach = objectMapper.readValue(req.getReader(), Coach.class);
                    if (coach == null)
                    {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        return;
                    }
                    if (!Validator.isValidPhoneNumber(coach.getPhoneNumber()) || coach.getPhoneNumber() == null || coach.getName() == null ||
                            coach.getLastName() == null || coach.getAddress() == null)
                    {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        return;
                    }
                    req.setAttribute("coach", coach);
                }
                catch (JsonMappingException e)
                {
                    LOGGER.error(ExceptionConstants.JSON_MAPPING_EXCEPTION_MESSAGE , e);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            }break;
        }
        chain.doFilter(req, resp);
    }

    @Override
    public void destroy()
    {

    }
}
