package com.isoft.intern.fitness.filters;


import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.intern.fitness.models.TrainingRegime;
import com.isoft.intern.fitness.utils.constants.ExceptionConstants;
import com.isoft.intern.fitness.utils.validator.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author: Nikolay Uzunov
 */
@WebFilter(filterName = "TrainingRegimeFilter", urlPatterns = "/regime")
public class TrainingRegimeFilter implements Filter
{
    private static final Logger LOGGER = LogManager.getLogger(TrainingRegime.class.getName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        ObjectMapper objectMapper = new ObjectMapper();

        switch (req.getMethod().toUpperCase())
        {
            case "GET":
            {
                String regimeTypeId = req.getParameter("trainingRegimeTypeId");
                String rate = req.getParameter("rate");
                String coachId = req.getParameter("coachId");

                if (!Validator.isInteger(regimeTypeId) || !Validator.isInteger(rate) || !Validator.isInteger(coachId))
                {
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }

                //TODO: CHECK FOR EXISTING ID'S
                if (Integer.parseInt(regimeTypeId) == 0 || Integer.parseInt(rate) < 0 || Integer.parseInt(rate) > 10 ||
                        Integer.parseInt(coachId) == 0)
                {
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }

                TrainingRegime trainingRegime = new TrainingRegime();
                trainingRegime.setTrainingRegimeTypeId(Integer.parseInt(regimeTypeId));
                trainingRegime.setCoachId(Integer.parseInt(coachId));
                trainingRegime.setRate(Integer.parseInt(rate));

                req.setAttribute("regime", trainingRegime);
            }
            break;
            case "POST":
            {
                try
                {
                    TrainingRegime trainingRegime = objectMapper.readValue(req.getReader(), TrainingRegime.class);
                    if (trainingRegime.getCoachId() == 0 || trainingRegime.getTrainingRegimeTypeId() == 0 ||
                            trainingRegime.getRate() < 0 || trainingRegime.getRate() > 10 || trainingRegime.getRegime() == null)
                    {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        return;
                    }
                    req.setAttribute("regime", trainingRegime);
                }
                catch (JsonMappingException e)
                {
                    LOGGER.error(ExceptionConstants.JSON_MAPPING_EXCEPTION_MESSAGE , e);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            }
            break;
            case "PUT":
            {
                try
                {
                    TrainingRegime trainingRegime = objectMapper.readValue(req.getReader(), TrainingRegime.class);
                    //TODO: CHECK FOR EXISTING ID'S
                    if (trainingRegime.getId() == 0 || trainingRegime.getCoachId() == 0 ||
                            trainingRegime.getTrainingRegimeTypeId() == 0 || trainingRegime.getRate() < 0 ||
                            trainingRegime.getRate() > 10 || trainingRegime.getRegime() == null)
                    {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        return;
                    }
                    req.setAttribute("regime", trainingRegime);
                }
                catch (JsonMappingException e)
                {
                    LOGGER.error(ExceptionConstants.JSON_MAPPING_EXCEPTION_MESSAGE , e);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            }
            break;
            case "DELETE":
            {
                try
                {
                    TrainingRegime trainingRegime = objectMapper.readValue(req.getReader(), TrainingRegime.class);
                    //TODO: CHECK FOR EXISTING ID'S
                    if (trainingRegime.getId() == 0)
                    {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        return;
                    }
                    req.setAttribute("regime", trainingRegime);
                }
                catch (JsonMappingException e)
                {
                    LOGGER.error(ExceptionConstants.JSON_MAPPING_EXCEPTION_MESSAGE , e);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            }
            break;
        }
        chain.doFilter(req, resp);
    }

    @Override
    public void destroy()
    {

    }
}
