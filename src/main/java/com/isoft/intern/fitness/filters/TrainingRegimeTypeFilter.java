package com.isoft.intern.fitness.filters;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.intern.fitness.models.TrainingRegimeType;
import com.isoft.intern.fitness.utils.constants.ExceptionConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author: Nikolay Uzunov
 */
@WebFilter(filterName = "TrainingRegimeTypeFilter", urlPatterns = "/regimeType")
public class TrainingRegimeTypeFilter implements Filter
{
    private static final Logger LOGGER = LogManager.getLogger(TrainingRegimeType.class.getName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        ObjectMapper objectMapper = new ObjectMapper();

        switch (req.getMethod().toUpperCase())
        {
            case "POST":
            {
                try
                {
                    TrainingRegimeType trainingRegimeType = objectMapper.readValue(req.getReader(), TrainingRegimeType.class);

                    if (trainingRegimeType.getType() == null || trainingRegimeType.getType().equals(""))
                    {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }

                    req.setAttribute("regimeType", trainingRegimeType);
                }
                catch (JsonMappingException e)
                {
                    LOGGER.error(ExceptionConstants.JSON_MAPPING_EXCEPTION_MESSAGE , e);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            }break;
            case "PUT":
            {
                try
                {
                    TrainingRegimeType trainingRegimeType = objectMapper.readValue(req.getReader(), TrainingRegimeType.class);

                    //TODO: CHECK FOR EXISTING ID
                    if (trainingRegimeType.getId() == 0 || trainingRegimeType.getType() == null ||
                            trainingRegimeType.getType().equals(""))
                    {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }

                    req.setAttribute("regimeType", trainingRegimeType);
                }
                catch (JsonMappingException e)
                {
                    LOGGER.error(ExceptionConstants.JSON_MAPPING_EXCEPTION_MESSAGE , e);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            }break;
            case "DELETE":
            {
                try
                {
                    TrainingRegimeType trainingRegimeType = objectMapper.readValue(req.getReader(), TrainingRegimeType.class);

                    //TODO: CHECK FOR EXISTING ID
                    if (trainingRegimeType.getId() == 0)
                    {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }

                    req.setAttribute("regimeType", trainingRegimeType);
                }
                catch (JsonMappingException e)
                {
                    LOGGER.error(ExceptionConstants.JSON_MAPPING_EXCEPTION_MESSAGE , e);
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            }
        }
        chain.doFilter(req, resp);
    }

    @Override
    public void destroy()
    {

    }
}
