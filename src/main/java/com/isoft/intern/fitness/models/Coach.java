package com.isoft.intern.fitness.models;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public class Coach
{
    private int id;
    private int experience;
    private String name;
    private String lastName;
    private String phoneNumber;
    private String address;

    private List<TrainingRegime> trainingRegimes;

    public Coach()
    {
    }

    public int getCoachId()
    {
        if (!trainingRegimes.isEmpty())
        {
            return trainingRegimes.get(0).getCoachId();
        }
        else return -1;
    }
    public Coach(int experience, String name, String lastName, String phoneNumber, String address)
    {
        this.experience = experience;
        this.name = name;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.trainingRegimes = new ArrayList<>();
    }

    public Coach(int id, int experience, String name, String lastName, String phoneNumber, String address)
    {
        this(experience, name, lastName, phoneNumber, address);
        this.id = id;
    }

    public void setTrainingRegimes(List<TrainingRegime> trainingRegimes)
    {
        this.trainingRegimes = trainingRegimes;
    }

    public List<TrainingRegime> getTrainingRegimes()
    {
        return trainingRegimes;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getExperience()
    {
        return experience;
    }

    public void setExperience(int experience)
    {
        this.experience = experience;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    @Override
    public String toString()
    {
        return super.toString();
    }
}
