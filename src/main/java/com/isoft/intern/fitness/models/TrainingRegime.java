package com.isoft.intern.fitness.models;

/**
 * @author Nikolay Uzunov
 */
public class TrainingRegime
{
    private int id;
    private int trainingRegimeTypeId;
    private int coachId;
    private int rate;
    private String regime;

    public TrainingRegime()
    {
    }

    public TrainingRegime(int trainingRegimeTypeId, int coachId, int rate, String regime)
    {
        this.trainingRegimeTypeId = trainingRegimeTypeId;
        this.coachId = coachId;
        this.rate = rate;
        this.regime = regime;
    }

    public TrainingRegime(int id, int trainingRegimeTypeId, int coachId, int rate, String regime)
    {
        this(trainingRegimeTypeId, coachId, rate, regime);
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getTrainingRegimeTypeId()
    {
        return trainingRegimeTypeId;
    }

    public void setTrainingRegimeTypeId(int trainingRegimeTypeId)
    {
        this.trainingRegimeTypeId = trainingRegimeTypeId;
    }

    public int getCoachId()
    {
        return coachId;
    }

    public void setCoachId(int coachId)
    {
        this.coachId = coachId;
    }

    public int getRate()
    {
        return rate;
    }

    public void setRate(int rate)
    {
        this.rate = rate;
    }

    public String getRegime()
    {
        return regime;
    }

    public void setRegime(String regime)
    {
        this.regime = regime;
    }

    @Override
    public String toString()
    {
        return "TrainingRegimeTypeRepositoryImpl{" +
                "id=" + id +
                ", trainingRegimeTypeId=" + trainingRegimeTypeId +
                ", coachId=" + coachId +
                ", rate=" + rate +
                ", regime='" + regime + '\'' +
                '}';
    }
}
