package com.isoft.intern.fitness.models;

/**
 * @author Nikolay Uzunov
 */
public class TrainingRegimeType
{
    private int id;
    private String type;

    public TrainingRegimeType()
    {
    }

    public TrainingRegimeType(String type)
    {
        this.type = type;
    }

    public TrainingRegimeType(int id, String type)
    {
        this(type);
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "TrainingRegimeType{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}
