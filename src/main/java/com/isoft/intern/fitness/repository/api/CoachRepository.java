package com.isoft.intern.fitness.repository.api;

import com.isoft.intern.fitness.models.Coach;

import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public interface CoachRepository extends Repository<Coach>
{
    List<Coach> getByName(String name);
    List<Coach> getByLastName(String lastName);
    List<Coach> getByFullName(String name, String lastName);
}
