package com.isoft.intern.fitness.repository.api;

import java.util.List;

/**
 * @author: Nikolay Uzunov
 */
public interface Repository<T>
{
    T getById(int id);
    List<T> getAll();
    void save(T entity);
    void update(T entity);
    void delete(T entity);
}
