package com.isoft.intern.fitness.repository.api;

import com.isoft.intern.fitness.models.TrainingRegime;

import java.util.List;

/**
 * @author: Nikolay Uzunov
 */
public interface TrainingRegimeRepository extends Repository<TrainingRegime>
{
    List<TrainingRegime> getTrainingRegimes(
            int trainingRegimeTypeId, int coachId, int rate);

    List<TrainingRegime> getByTrainingRegimeTypeIdAndByCoach(
            int trainingRegimeTypeId, int coachId);

    List<TrainingRegime> getByRateAndByTrainingRegimeTypeId(
            int rate, int trainingRegimeTypeId);

    List<TrainingRegime> getByCoachAndByRate(int coachId, int rate);

    List<TrainingRegime> getByTrainingRegimeTypeId(int trainingRegimeTypeId);

    List<TrainingRegime> getByRate(int rate);

    List<TrainingRegime> getByCoachId(int coachId);
}
