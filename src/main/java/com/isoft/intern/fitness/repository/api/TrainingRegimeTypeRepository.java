package com.isoft.intern.fitness.repository.api;

import com.isoft.intern.fitness.models.TrainingRegimeType;

import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public interface TrainingRegimeTypeRepository extends Repository<TrainingRegimeType>
{
    List<TrainingRegimeType> getByType(String type);
}
