package com.isoft.intern.fitness.repository.impl;

import com.isoft.intern.fitness.dbconnector.PostgreSQLConnector;
import com.isoft.intern.fitness.models.Coach;
import com.isoft.intern.fitness.models.TrainingRegime;
import com.isoft.intern.fitness.repository.api.CoachRepository;
import com.isoft.intern.fitness.repository.api.TrainingRegimeRepository;
import com.isoft.intern.fitness.utils.constants.ExceptionConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public class CoachRepositoryImpl implements CoachRepository
{

    private static Connection CONNECTION = PostgreSQLConnector.getConnection();
    private static Logger LOGGER = LogManager.getLogger(Coach.class.getName());
    private static final String GET_USERS_BY_NAME_QUERY = "SELECT * " +
            "FROM public.coaches WHERE name = ?;";
    private static final String GET_USERS_BY_LAST_NAME_QUERY = "SELECT * " +
            "FROM public.coaches WHERE last_name = ?;";
    private static final String GET_USERS_BY_FULL_NAME = "SELECT * FROM public.coaches " +
            "WHERE name = ? " +
            "AND last_name = ?;";
    private static final String INSERT_QUERY = "INSERT INTO public.coaches(" +
            "name, last_name, phone_number, experience, address)\n" +
            "    VALUES ( ?, ?, ?, ?, ?);";
    private static final String GET_ALL_QUERY = "SELECT * FROM public.coaches;";
    private static final String UPDATE_QUERY = "UPDATE public.coaches\n" +
            "\tSET name=?, last_name=?, phone_number=?, experience=?, address=? WHERE id=?;";
    private static final String GET_BY_ID_QUERY = "SELECT * FROM public.coaches WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM public.coaches\n" +
            "\tWHERE id=?;";

    private static List<Coach> coaches;
    private List<TrainingRegime> trainingRegimes;
    private static final TrainingRegimeRepository trainingRegimeRepository =
            new TrainingRegimeRepositoryImpl();

    @Override
    public Coach getById(int id)
    {
        Coach coach = new Coach();
        try(PreparedStatement preparedStatement = CONNECTION.prepareStatement(GET_BY_ID_QUERY);)
        {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet != null)
            {
                while (resultSet.next())
                {
                    coach.setId(resultSet.getInt("id"));
                    coach.setName(resultSet.getString("name"));
                    coach.setLastName(resultSet.getString("last_name"));
                    coach.setPhoneNumber(resultSet.getString("phone_number"));
                    coach.setExperience(resultSet.getInt("experience"));
                    coach.setAddress(resultSet.getString("address"));
                }
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
        return coach;
    }

    @Override
    public List<Coach> getAll()
    {
        coaches = new ArrayList<>();

        try(PreparedStatement preparedStatement = CONNECTION.prepareStatement(GET_ALL_QUERY))
        {
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet != null)
            {
                Coach coach;
                while (resultSet.next())
                {
                    coach = new Coach(
                            resultSet.getInt("id"),
                            resultSet.getInt("experience"),
                            resultSet.getString("name"),
                            resultSet.getString("last_name"),
                            resultSet.getString("phone_number"),
                            resultSet.getString("address")
                    );
                    coach.setTrainingRegimes(trainingRegimeRepository.getByCoachId(coach.getId()));
                    coaches.add(coach);
                }
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
        return coaches;
    }

    @Override
    public void save(Coach entity)
    {
        try(PreparedStatement preparedStatement =
                    CONNECTION.prepareStatement(INSERT_QUERY))
        {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getLastName());
            preparedStatement.setString(3, entity.getPhoneNumber());
            preparedStatement.setInt(4, entity.getExperience());
            preparedStatement.setString(5, entity.getAddress());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error("SQL Exception", e);
        }
    }

    @Override
    public void update(Coach entity)
    {
        try(PreparedStatement preparedStatement =
                    CONNECTION.prepareStatement(UPDATE_QUERY))
        {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getLastName());
            preparedStatement.setString(3, entity.getPhoneNumber());
            preparedStatement.setInt(4, entity.getExperience());
            preparedStatement.setString(5, entity.getAddress());
            preparedStatement.setInt(6, entity.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
    }

    @Override
    public void delete(Coach entity)
    {
        try(PreparedStatement preparedStatement = CONNECTION.prepareStatement(DELETE_QUERY);)
        {
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
    }

    public List<Coach> getByName(String name)
    {
        return getCoaches(name, GET_USERS_BY_NAME_QUERY);
    }

    public List<Coach> getByLastName(String lastName)
    {
        return getCoaches(lastName, GET_USERS_BY_LAST_NAME_QUERY);
    }

    private List<Coach> getCoaches(String someName, String query)
    {
        coaches = new ArrayList<>();
        try(PreparedStatement preparedStatement = CONNECTION.prepareStatement(query))
        {
            preparedStatement.setString(1, someName);
            deserializeCoach(preparedStatement);
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
        return coaches;
    }

    public List<Coach> getByFullName(String name, String lastName)
    {
        coaches = new ArrayList<>();
        try(PreparedStatement preparedStatement = CONNECTION.prepareStatement(GET_USERS_BY_FULL_NAME))
        {
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, lastName);
            deserializeCoach(preparedStatement);
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
        return coaches;
    }

    private void deserializeCoach(PreparedStatement preparedStatement) throws SQLException
    {
        ResultSet resultSet = preparedStatement.executeQuery();

        if (resultSet != null)
        {
            while (resultSet.next())
            {
                Coach coach = new Coach(
                        resultSet.getInt("id"),
                        resultSet.getInt("experience"),
                        resultSet.getString("name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("phone_number"),
                        resultSet.getString("address")
                );
                coach.setTrainingRegimes(trainingRegimeRepository.getByCoachId(coach.getId()));
                coaches.add(coach);
            }
        }
    }
}
