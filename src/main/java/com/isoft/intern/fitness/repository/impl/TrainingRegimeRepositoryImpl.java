package com.isoft.intern.fitness.repository.impl;

import com.isoft.intern.fitness.dbconnector.PostgreSQLConnector;
import com.isoft.intern.fitness.models.TrainingRegime;
import com.isoft.intern.fitness.repository.api.CoachRepository;
import com.isoft.intern.fitness.repository.api.TrainingRegimeRepository;
import com.isoft.intern.fitness.utils.constants.ExceptionConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public class TrainingRegimeRepositoryImpl implements TrainingRegimeRepository
{
    private static final Connection CONNECTION = PostgreSQLConnector.getConnection();
    private static final Logger LOGGER = LogManager
            .getLogger(TrainingRegimeRepositoryImpl.class.getName());
    private static final CoachRepository coachRepository = new CoachRepositoryImpl();
    private static final String INSERT_QUERY =
            "INSERT INTO public.training_regimes(rate, regime, training_regime_type_id, coach_id)" +
                    "VALUES (?, ?, ?, ?);";
    private static final String GET_BY_ID_QUERY =
            "SELECT * " +
                    "FROM public.training_regimes WHERE id = ?;";
    private static final String GET_ALL_QUERY =
            "SELECT * " +
                    "FROM public.training_regimes";
    private static final String UPDATE_QUERY =
            "UPDATE public.training_regimes\n" +
                    "SET rate=?, regime=?, training_regime_type_id=?, coach_id=? " +
                    "WHERE id=?;";
    private static final String DELETE_QUERY = "" +
            "DELETE " +
            "FROM public.training_regimes " +
            "WHERE id=?;";
    private static final String GET_BY_COACH_ID_QUERY =
            "SELECT * " +
                    "FROM public.training_regimes " +
                    "WHERE coach_id = ?;";
    private static final String GET_BY_RATE_QUERY =
            "SELECT * " +
                    "FROM public.training_regimes " +
                    "WHERE rate = ?;";
    private static final String GET_BY_TRAINING_REGIME_ID_QUERY =
            "SELECT * " +
                    "FROM public.training_regimes " +
                    "WHERE training_regime_type_id = ?;";
    private static final String GET_BY_COACH_ID_AND_TRAINING_REGIME_ID_QUERY =
            "SELECT * " +
                    "FROM public.training_regimes " +
                    "WHERE coach_id = ? AND training_regime_type_id = ?;";
    private static final String GET_BY_COACH_AND_RATE_QUERY =
            "SELECT * " +
                    "FROM public.training_regimes " +
                    "WHERE coach_id =? AND rate = ?;";
    private static final String GET_BY_RATE_AND_TRAINING_REGIME_TYPE_ID_QUERY =
            "SELECT * " +
                    "FROM public.training_regimes " +
                    "WHERE training_regime_type_id = ? AND rate =?;";
    private static final String GET_BY_TRAINING_REGIME_TYPE_ID_AND_COACH_AND_RATE_QUERY =
            "SELECT * " +
                    "FROM public.training_regimes " +
                    "WHERE training_regime_type_id = ? AND coach_id = ? AND rate = ? ;";
    private List<TrainingRegime> trainingRegimes;

    @Override
    public TrainingRegime getById(int id)
    {
        TrainingRegime trainingRegime = new TrainingRegime();
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(GET_BY_ID_QUERY))
        {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next())
            {
                trainingRegime.setId(resultSet.getInt("id"));
                trainingRegime.setCoachId(resultSet.getInt("coach_id"));
                trainingRegime.setTrainingRegimeTypeId(resultSet.getInt("training_regime_type_id"));
                trainingRegime.setRate(resultSet.getInt("rate"));
                trainingRegime.setRegime(resultSet.getString("regime"));
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }

        return trainingRegime;
    }

    @Override
    public List<TrainingRegime> getAll()
    {
        List<TrainingRegime> trainingRegimes = new ArrayList<>();

        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(GET_ALL_QUERY))
        {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next())
            {
                TrainingRegime trainingRegime =new TrainingRegime(
                        resultSet.getInt("id"),
                        resultSet.getInt("training_regime_type_id"),
                        resultSet.getInt("coach_id"),
                        resultSet.getInt("rate"),
                        resultSet.getString("regime"));
                trainingRegimes.add(trainingRegime);
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
        return trainingRegimes;
    }

    @Override
    public void save(TrainingRegime entity)
    {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(INSERT_QUERY))
        {
            preparedStatement.setInt(1, entity.getRate());
            preparedStatement.setString(2, entity.getRegime());
            preparedStatement.setInt(3, entity.getTrainingRegimeTypeId());
            preparedStatement.setInt(4, entity.getCoachId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error("PreparedStatement/SQL Exception: ", e);
        }
    }

    @Override
    public void update(TrainingRegime entity)
    {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(UPDATE_QUERY))
        {
            preparedStatement.setInt(1, entity.getRate());
            preparedStatement.setString(2, entity.getRegime());
            preparedStatement.setInt(3, entity.getTrainingRegimeTypeId());
            preparedStatement.setInt(4, entity.getCoachId());
            preparedStatement.setInt(5, entity.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
    }

    @Override
    public void delete(TrainingRegime entity)
    {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(DELETE_QUERY))
        {
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
    }

    @Override
    public List<TrainingRegime> getByCoachId(int coachId)
    {
        return getTrainingRegimes(coachId, GET_BY_COACH_ID_QUERY);
    }

    @Override
    public List<TrainingRegime> getByRate(int rate)
    {
        return getTrainingRegimes(rate, GET_BY_RATE_QUERY);
    }

    @Override
    public List<TrainingRegime> getByTrainingRegimeTypeId(int trainingRegimeTypeId)
    {
        return getTrainingRegimes(trainingRegimeTypeId, GET_BY_TRAINING_REGIME_ID_QUERY);
    }

    @Override
    public List<TrainingRegime> getByCoachAndByRate(int coachId, int rate)
    {
        return getTrainingRegimes(coachId, rate, GET_BY_COACH_AND_RATE_QUERY);
    }

    @Override
    public List<TrainingRegime> getByRateAndByTrainingRegimeTypeId(int rate, int trainingRegimeTypeId)
    {
        return getTrainingRegimes(trainingRegimeTypeId, rate, GET_BY_RATE_AND_TRAINING_REGIME_TYPE_ID_QUERY);
    }

    @Override
    public List<TrainingRegime> getByTrainingRegimeTypeIdAndByCoach(int coachId, int trainingRegimeTypeId)
    {
        return getTrainingRegimes(trainingRegimeTypeId, coachId, GET_BY_COACH_ID_AND_TRAINING_REGIME_ID_QUERY);
    }

    private List<TrainingRegime> getTrainingRegimes(int coachId, String query)
    {
        trainingRegimes = new ArrayList<>();
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(query))
        {
            preparedStatement.setInt(1, coachId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next())
            {
                TrainingRegime trainingRegime = new TrainingRegime(
                        resultSet.getInt("id"),
                        resultSet.getInt("training_regime_type_id"),
                        resultSet.getInt("coach_id"),
                        resultSet.getInt("rate"),
                        resultSet.getString("regime"));
                trainingRegimes.add(trainingRegime);
            }
        }
        catch (SQLException e)

        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
        return trainingRegimes;
    }

    private List<TrainingRegime> getTrainingRegimes(int filterParam1, int filterParam2, String query)
    {
        trainingRegimes = new ArrayList<>();
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(query))
        {
            preparedStatement.setInt(1, filterParam1);
            preparedStatement.setInt(2, filterParam2);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next())
            {
                trainingRegimes.add(new TrainingRegime(
                        resultSet.getInt("id"),
                        resultSet.getInt("training_regime_type_id"),
                        resultSet.getInt("coach_id"),
                        resultSet.getInt("rate"),
                        resultSet.getString("regime")
                ));
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
        return trainingRegimes;
    }

    @Override
    public List<TrainingRegime> getTrainingRegimes(int trainingRegimeTypeId, int coachId, int rate)
    {
        trainingRegimes = new ArrayList<>();
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(GET_BY_TRAINING_REGIME_TYPE_ID_AND_COACH_AND_RATE_QUERY))
        {
            preparedStatement.setInt(1, trainingRegimeTypeId);
            preparedStatement.setInt(2, coachId);
            preparedStatement.setInt(3, rate);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next())
            {
                trainingRegimes.add(new TrainingRegime(
                        resultSet.getInt("id"),
                        resultSet.getInt("training_regime_type_id"),
                        resultSet.getInt("coach_id"),
                        resultSet.getInt("rate"),
                        resultSet.getString("regime")
                ));
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
        return trainingRegimes;
    }
}
