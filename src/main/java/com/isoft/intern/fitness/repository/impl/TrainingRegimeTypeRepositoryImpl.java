package com.isoft.intern.fitness.repository.impl;

import com.isoft.intern.fitness.dbconnector.PostgreSQLConnector;
import com.isoft.intern.fitness.models.TrainingRegimeType;
import com.isoft.intern.fitness.repository.api.TrainingRegimeTypeRepository;
import com.isoft.intern.fitness.utils.constants.ExceptionConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public class TrainingRegimeTypeRepositoryImpl implements TrainingRegimeTypeRepository
{
    private static final Connection CONNECTION = PostgreSQLConnector.getConnection();
    private static final Logger LOGGER = LogManager
            .getLogger(TrainingRegimeTypeRepositoryImpl.class.getName());
    private static final String GET_BY_ID = "SELECT * FROM public.training_regime_types WHERE id = ?;";
    private static final String GET_ALL_QUERY = "SELECT * FROM public.training_regime_types";
    private static final String INSERT_QUERY = "INSERT INTO public.training_regime_types(" +
            "type)" +
            "VALUES (?);";
    private static final String UPDATE_QUERY = "UPDATE public.training_regime_types SET type=?" +
            "WHERE id = ?;";
    private static final String DELETE_QUERY = "DELETE FROM public.training_regime_types WHERE id = ?;";
    private static final String GET_BY_TYPE_QUERY = "SELECT * FROM public.training_regime_types WHERE type =?;";
    private static List<TrainingRegimeType> trainingRegimeTypes;

    @Override
    public TrainingRegimeType getById(int id)
    {
        TrainingRegimeType trainingRegimeType = new TrainingRegimeType();

        try(PreparedStatement preparedStatement = CONNECTION.prepareStatement(GET_BY_ID))
        {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            
            if (resultSet.next())
            {
                trainingRegimeType.setId(resultSet.getInt("id"));
                trainingRegimeType.setType(resultSet.getString("type"));
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
        return trainingRegimeType;
    }

    @Override
    public List<TrainingRegimeType> getAll()
    {
        List<TrainingRegimeType> trainingRegimeTypes = new ArrayList<>();
        try(PreparedStatement preparedStatement = CONNECTION.prepareStatement(GET_ALL_QUERY))
        {
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while (resultSet.next())
            {
                trainingRegimeTypes.add(new TrainingRegimeType(
                        resultSet.getInt("id"),
                        resultSet.getString("type")
                ));
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
        return trainingRegimeTypes;
    }

    @Override
    public void save(TrainingRegimeType entity)
    {
        try(PreparedStatement preparedStatement = CONNECTION.prepareStatement(INSERT_QUERY))
        {
            preparedStatement.setString(1, entity.getType());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
    }

    @Override
    public void update(TrainingRegimeType entity)
    {
        try(PreparedStatement preparedStatement = CONNECTION.prepareStatement(UPDATE_QUERY))
        {
            preparedStatement.setString(1, entity.getType());
            preparedStatement.setInt(2, entity.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
    }

    @Override
    public void delete(TrainingRegimeType entity)
    {
        try(PreparedStatement preparedStatement = CONNECTION.prepareStatement(DELETE_QUERY))
        {
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
    }

    public List<TrainingRegimeType> getByType(String type)
    {
        trainingRegimeTypes = new ArrayList<>();
        try(PreparedStatement preparedStatement = CONNECTION.prepareStatement(GET_BY_TYPE_QUERY))
        {
            preparedStatement.setString(1, type);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next())
            {
                trainingRegimeTypes.add(new TrainingRegimeType(resultSet.getInt("id"),
                        resultSet.getString("type")));
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(ExceptionConstants.SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE, e);
        }
        return trainingRegimeTypes;
    }
}
