package com.isoft.intern.fitness.services.api;

import java.util.List;

/**
 * @author: Nikolay Uzunov
 */
public interface Service<T>
{
    List<T> get(T entity);
    List<T> getAll();
    void save(T entity);
    void update(T entity);
    void delete(T entity);
}
