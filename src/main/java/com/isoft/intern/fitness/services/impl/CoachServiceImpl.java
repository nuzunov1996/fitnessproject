package com.isoft.intern.fitness.services.impl;

import com.isoft.intern.fitness.filters.CoachFilter;
import com.isoft.intern.fitness.models.Coach;
import com.isoft.intern.fitness.repository.api.CoachRepository;
import com.isoft.intern.fitness.repository.api.TrainingRegimeRepository;
import com.isoft.intern.fitness.repository.impl.CoachRepositoryImpl;
import com.isoft.intern.fitness.repository.impl.TrainingRegimeRepositoryImpl;
import com.isoft.intern.fitness.services.api.Service;

import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public class CoachServiceImpl implements Service<Coach>
{
    private static final CoachRepository coachRepository = new CoachRepositoryImpl();
    private static List<Coach> coaches;

    @Override
    public List<Coach> get(Coach entity)
    {
        //Get by name
        if (entity.getName() != null && entity.getLastName() == null)
        {
            coaches = coachRepository.getByName(entity.getName());
            return coaches;
        }
        //Get by last name
        if (entity.getLastName() != null && entity.getName() == null)
        {
            coaches = coachRepository.getByLastName(entity.getLastName());
            return coaches;
        }
        //Get by full name
        if (entity.getName() != null && entity.getLastName() != null)
        {
            coaches = coachRepository.getByFullName(entity.getName(), entity.getLastName());
            return coaches;
        }
        return coachRepository.getAll();
    }

    @Override
    public List<Coach> getAll()
    {
        return coachRepository.getAll();
    }

    @Override
    public void save(Coach entity)
    {
        coachRepository.save(entity);
    }

    @Override
    public void update(Coach entity)
    {
        coachRepository.update(entity);
    }

    @Override
    public void delete(Coach entity)
    {
        coachRepository.delete(entity);
    }
}
