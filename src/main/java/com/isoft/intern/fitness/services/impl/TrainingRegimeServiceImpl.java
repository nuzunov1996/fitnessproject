package com.isoft.intern.fitness.services.impl;

import com.isoft.intern.fitness.filters.TrainingRegimeFilter;
import com.isoft.intern.fitness.models.Coach;
import com.isoft.intern.fitness.models.TrainingRegime;
import com.isoft.intern.fitness.repository.api.CoachRepository;
import com.isoft.intern.fitness.repository.api.TrainingRegimeRepository;
import com.isoft.intern.fitness.repository.api.TrainingRegimeTypeRepository;
import com.isoft.intern.fitness.repository.impl.CoachRepositoryImpl;
import com.isoft.intern.fitness.repository.impl.TrainingRegimeRepositoryImpl;
import com.isoft.intern.fitness.repository.impl.TrainingRegimeTypeRepositoryImpl;
import com.isoft.intern.fitness.services.api.Service;

import java.util.*;

/**
 * @author: Nikolay Uzunov
 */
public class TrainingRegimeServiceImpl implements Service<TrainingRegime>
{
    private static final TrainingRegimeRepository trainingRegimeRepository =
            new TrainingRegimeRepositoryImpl();
    private static List<TrainingRegime> trainingRegimes;
    @Override
    public List<TrainingRegime> get(TrainingRegime trainingRegime)
    {
        //SINGLE FILTERING
        //Get by coach id.
        if (trainingRegime.getCoachId() != 0 && trainingRegime.getRate() == 0
                && trainingRegime.getTrainingRegimeTypeId() == 0)
        {
            trainingRegimes = trainingRegimeRepository.getByCoachId(trainingRegime.getCoachId());
            return trainingRegimes;
        }
        //Get by rate.
        if (trainingRegime.getRate() != 0 && trainingRegime.getTrainingRegimeTypeId() == 0
                && trainingRegime.getCoachId() == 0)
        {
            trainingRegimes = trainingRegimeRepository.getByRate(trainingRegime.getRate());
            return trainingRegimes;
        }
        //Get by training regime type id.
        if (trainingRegime.getTrainingRegimeTypeId() != 0 && trainingRegime.getRate() == 0
                && trainingRegime.getCoachId() == 0)
        {
            trainingRegimes = trainingRegimeRepository.getByTrainingRegimeTypeId(
                    trainingRegime.getTrainingRegimeTypeId());
            return trainingRegimes;
        }

        //DOUBLE FILTERING
        //Get by coach id and by rate
        if (trainingRegime.getCoachId() != 0 && trainingRegime.getRate() != 0
                && trainingRegime.getTrainingRegimeTypeId() == 0)
        {
            trainingRegimes = trainingRegimeRepository.getByCoachAndByRate(trainingRegime.getCoachId(),
                    trainingRegime.getRate());
            return trainingRegimes;
        }
        //Get by rate and by coach id.
        if (trainingRegime.getRate() != 0 && trainingRegime.getTrainingRegimeTypeId() != 0
                && trainingRegime.getCoachId() == 0)
        {
            trainingRegimes = trainingRegimeRepository.getByRateAndByTrainingRegimeTypeId(trainingRegime.getRate(),
                    trainingRegime.getTrainingRegimeTypeId());
            return trainingRegimes;
        }
        //Get by training regime type id and by coach id
        if (trainingRegime.getTrainingRegimeTypeId() != 0 && trainingRegime.getRate() == 0
                && trainingRegime.getCoachId() != 0)
        {
            trainingRegimes = trainingRegimeRepository.getByTrainingRegimeTypeIdAndByCoach(
                    trainingRegime.getTrainingRegimeTypeId(), trainingRegime.getCoachId());
            return trainingRegimes;
        }

        //TRIPLE FILTERING
        //Get by training regime type id, by rate and by coach id.
        if (trainingRegime.getTrainingRegimeTypeId() != 0 && trainingRegime.getRate() != 0
                && trainingRegime.getCoachId() != 0)
        {
            trainingRegimes = trainingRegimeRepository.getTrainingRegimes(
                    trainingRegime.getTrainingRegimeTypeId(), trainingRegime.getCoachId(),
                    trainingRegime.getRate());
            return trainingRegimes;
        }
        return trainingRegimeRepository.getAll();
    }

    @Override
    public List<TrainingRegime> getAll()
    {
        return trainingRegimeRepository.getAll();
    }

    @Override
    public void save(TrainingRegime entity)
    {
        trainingRegimeRepository.save(entity);
    }

    @Override
    public void update(TrainingRegime entity)
    {
        trainingRegimeRepository.update(entity);
    }

    @Override
    public void delete(TrainingRegime entity)
    {
        trainingRegimeRepository.delete(entity);
    }
}
