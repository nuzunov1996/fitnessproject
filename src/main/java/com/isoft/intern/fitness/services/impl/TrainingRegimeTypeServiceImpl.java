package com.isoft.intern.fitness.services.impl;

import com.isoft.intern.fitness.filters.TrainingRegimeTypeFilter;
import com.isoft.intern.fitness.models.TrainingRegimeType;
import com.isoft.intern.fitness.repository.api.TrainingRegimeTypeRepository;
import com.isoft.intern.fitness.repository.impl.TrainingRegimeTypeRepositoryImpl;
import com.isoft.intern.fitness.services.api.Service;

import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public class TrainingRegimeTypeServiceImpl implements Service<TrainingRegimeType>
{
    private static final TrainingRegimeTypeRepository trainingRegimeTypeRepository =
            new TrainingRegimeTypeRepositoryImpl();

    @Override
    public List<TrainingRegimeType> get(TrainingRegimeType entity)
    {
        //Get by type
        if (entity.getType() != null)
        {
           return trainingRegimeTypeRepository.getByType(entity.getType());
        }
        return trainingRegimeTypeRepository.getAll();
    }

    @Override
    public List<TrainingRegimeType> getAll()
    {
        return trainingRegimeTypeRepository.getAll();
    }

    @Override
    public void save(TrainingRegimeType entity)
    {
        trainingRegimeTypeRepository.save(entity);
    }

    @Override
    public void update(TrainingRegimeType entity)
    {
        trainingRegimeTypeRepository.update(entity);
    }

    @Override
    public void delete(TrainingRegimeType entity)
    {
        trainingRegimeTypeRepository.delete(entity);
    }
}
