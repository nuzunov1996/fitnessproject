package com.isoft.intern.fitness.servlets;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.intern.fitness.models.Coach;
import com.isoft.intern.fitness.services.api.Service;
import com.isoft.intern.fitness.services.impl.CoachServiceImpl;
import com.isoft.intern.fitness.utils.constants.ExceptionConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public class CoachServlet extends HttpServlet
{
    private static final Service coachServiceImpl = new CoachServiceImpl();
    private static final Logger LOGGER = LogManager.getLogger(CoachServlet.class.getName());
    private String json;
    private PrintWriter out;
    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void init() throws ServletException
    {
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        List<Coach> coaches;
        Coach coach = new Coach();
        coach.setName(req.getParameter("name"));
        coach.setLastName(req.getParameter("lastName"));
        coaches = coachServiceImpl.get(coach);

        resp.setContentType("application/json");
        try
        {
            json = mapper.writeValueAsString(coaches);
            out = resp.getWriter();
        }
        catch (IOException e)
        {
            LOGGER.error(ExceptionConstants.IO_EXCEPTION_MESSAGE, e);
        }
        out.println(json);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Coach coach = (Coach) req.getAttribute("coach");
        coachServiceImpl.save(coach);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Coach coach = (Coach) req.getAttribute("coach");
        coachServiceImpl.update(coach);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Coach coach = (Coach) req.getAttribute("coach");
        coachServiceImpl.delete(coach);
    }

    @Override
    public void destroy()
    {
        super.destroy();
    }
}
