package com.isoft.intern.fitness.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.intern.fitness.filters.TrainingRegimeFilter;
import com.isoft.intern.fitness.models.TrainingRegime;
import com.isoft.intern.fitness.services.impl.TrainingRegimeServiceImpl;
import com.isoft.intern.fitness.utils.constants.ExceptionConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public class TrainingRegimeServlet extends HttpServlet
{
    private static final Logger LOGGER = LogManager.getLogger(TrainingRegimeServlet.class.getName());
    private String json;
    private PrintWriter out;
    private ObjectMapper mapper = new ObjectMapper();

    private static final TrainingRegimeServiceImpl trainingRegimeServiceImpl =
            new TrainingRegimeServiceImpl();

    @Override
    public void init() throws ServletException
    {
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        TrainingRegime trainingRegime = (TrainingRegime) req.getAttribute("regime");
        List<TrainingRegime> trainingRegimes = trainingRegimeServiceImpl.get(trainingRegime);

        resp.setContentType("application/json");
        try
        {
            json = mapper.writeValueAsString(trainingRegimes);
            out = resp.getWriter();
        } catch (IOException e)
        {
            LOGGER.error(ExceptionConstants.IO_EXCEPTION_MESSAGE, e);
        }
        out.println(json);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        TrainingRegime trainingRegime = (TrainingRegime) req.getAttribute("regime");
        trainingRegimeServiceImpl.save(trainingRegime);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        TrainingRegime trainingRegime = (TrainingRegime) req.getAttribute("regime");
        trainingRegimeServiceImpl.update(trainingRegime);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        TrainingRegime trainingRegime = (TrainingRegime) req.getAttribute("regime");
        trainingRegimeServiceImpl.delete(trainingRegime);
    }

    @Override
    public void destroy()
    {
        super.destroy();
    }
}
