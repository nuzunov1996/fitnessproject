package com.isoft.intern.fitness.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.intern.fitness.filters.TrainingRegimeTypeFilter;
import com.isoft.intern.fitness.models.TrainingRegimeType;
import com.isoft.intern.fitness.services.impl.TrainingRegimeTypeServiceImpl;
import com.isoft.intern.fitness.utils.constants.ExceptionConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public class TrainingRegimeTypeServlet extends HttpServlet
{
    private static final Logger LOGGER = LogManager.getLogger(TrainingRegimeTypeServlet.class.getName());
    private static final TrainingRegimeTypeServiceImpl trainingRegimeTypeService =
            new TrainingRegimeTypeServiceImpl();
    private String json;
    private PrintWriter out;
    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void init() throws ServletException
    {
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        TrainingRegimeType trainingRegimeType = new TrainingRegimeType();
        trainingRegimeType.setType(req.getParameter("type"));
        List<TrainingRegimeType> trainingRegimeTypes = trainingRegimeTypeService.get(trainingRegimeType);

        resp.setContentType("application/json");
        try
        {
            json = mapper.writeValueAsString(trainingRegimeTypes);
            out = resp.getWriter();
        } catch (IOException e)
        {
            LOGGER.error(ExceptionConstants.IO_EXCEPTION_MESSAGE, e);
        }
        out.println(json);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        TrainingRegimeType trainingRegimeType = (TrainingRegimeType) req.getAttribute("regimeType");
        trainingRegimeTypeService.save(trainingRegimeType);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        TrainingRegimeType trainingRegimeType = (TrainingRegimeType) req.getAttribute("regimeType");
        trainingRegimeTypeService.update(trainingRegimeType);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        TrainingRegimeType trainingRegimeType = (TrainingRegimeType) req.getAttribute("regimeType");
        trainingRegimeTypeService.delete(trainingRegimeType);
    }

    @Override
    public void destroy()
    {
        super.destroy();
    }
}
