package com.isoft.intern.fitness.utils.constants;

import java.util.ResourceBundle;

public class DataBaseConstants
{
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("dbconfiguration");
    public static String NAME = resourceBundle.getString("db.name");
    public static String USERNAME = resourceBundle.getString("db.username");
    public static String PASSWORD = resourceBundle.getString("db.password");
    public static String FQN = resourceBundle.getString("db.conn.url");
    public static String DRIVER = resourceBundle.getString("db.driver.class");
}
