package com.isoft.intern.fitness.utils.constants;

public class ExceptionConstants
{
    public static final String SQL_EXCEPTION_PREPARED_STATEMENT_MESSAGE = "PreparedStatement/SQL Exception";
    public static final String SQL_EXCEPTION_MESSAGE = "SQL Exception";
    public static final String IO_EXCEPTION_MESSAGE = "IOException";
    public static final String JSON_MAPPING_EXCEPTION_MESSAGE = "JsonMappingException";
}
