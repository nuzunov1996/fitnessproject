package com.isoft.intern.fitness.utils.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Pattern;

/**
 * @author Nikolay Uzunov
 */
public class Validator
{
    private static final Logger LOGGER = LogManager.getLogger(Validator.class.getName());

    public static boolean isInteger(String value)
    {
        try
        {
            int number = Integer.parseInt(value);
            return true;
        }
        catch (NumberFormatException ex)
        {
            return false;
        }
    }

    public static boolean isValidPhoneNumber(String phoneNumber)
    {
        String regex = "^(08[789][0-9]{7})$";
        return Pattern.compile(regex).matcher(phoneNumber).matches();
    }
}
